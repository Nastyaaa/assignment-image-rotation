#include "bmp.h"
#include "rotate.h"

#include <stdio.h>

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "usage main <input_file> <output_file>");
        return 1;
    }
    FILE *inFile = fopen(argv[1], "rb");
    if (inFile == NULL) {
        fprintf(stderr, "failed open input file %s", argv[1]);
        return 2;
    }

    struct image to_rotate;
    enum read_status readStatus = readBmp(inFile, &to_rotate);
    if (readStatus != READ_OK) {
        fprintf(stderr, "wrong bmp file");
        fclose(inFile);
        destructor(to_rotate);
        return readStatus;
    }
    fclose(inFile);

    FILE *outFile = fopen(argv[2], "wb");
    if (outFile == NULL) {
        fprintf(stderr, "failed open output file %s", argv[2]);
        fclose(inFile);
        return 3;
    }

    struct image res = rotate(&to_rotate);
    destructor(to_rotate);

    enum write_status writeStatus = writeBmp(outFile, &res);
    destructor(res);
    fclose(outFile);
    if (writeStatus != WRITE_OK) {
        fprintf(stderr, "failed write output file");
        return writeStatus;
    }

    return 0;
}
