#include "image.h"
#include "rotate.h"
#include <stdlib.h>

struct image rotate(const struct image *image) {
    const uint64_t n = image->width;
    const uint64_t m = image->height;
    struct image res = (struct image) {
            .height = n,
            .width = m,
            .data = malloc(sizeof(struct pixel) * n * m)
    };

    for (size_t i = 0; i < m; ++i) {
        for (size_t w = 0; w < n; ++w) {
            res.data[(m - i - 1) + w * m] = image->data[i * n + w];
        }
    }

    return res;
}
