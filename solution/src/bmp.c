#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>

int32_t get_padding(const struct image *image) {
    return (int32_t)(image->width % 4);
}

struct bmp_header constructor(const struct image *image) {
    uint64_t height = image->height;
    uint64_t width = image->width;
    struct bmp_header header = {
            .bfType = 0x4D42,
            .bfileSize = (sizeof(struct bmp_header) + height * width * sizeof(struct pixel)
                          + height * get_padding(image)),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = height * width * sizeof(struct pixel) + get_padding(image) * height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}


enum read_status readBmp(FILE *in, struct image *image) {
    struct bmp_header *header = malloc(sizeof(struct bmp_header));

    fread(header, sizeof(struct bmp_header), 1, in);

    if (header->bfType != 0x4D42) {
        free(header);
        return READ_INVALID_SIGNATURE;
    }
    if (header->biSize != 40) {
        free(header);
        return READ_INVALID_HEADER;
    }
    if (header->biBitCount != 24) {
        free(header);
        return READ_INVALID_BITS;
    }


    if (header->biWidth > 0 && header->biHeight > 0) {
        *image = (struct image) {
                .width = header->biWidth,
                .height = header->biHeight,
                .data =malloc(sizeof(struct pixel) * header->biWidth * header->biHeight)
        };
    } else {
        free(header);
        return READ_INVALID_BITS;
    }

    for (uint32_t line = 0; line < header->biHeight; ++line) {
        fread(&(image->data[line * image->width]), sizeof(struct pixel), header->biWidth, in);
        fseek(in, get_padding(image), SEEK_CUR);
    }

    free(header);

    return READ_OK;
}

enum write_status writeBmp(FILE *out, const struct image *image) {
    struct bmp_header header = constructor(image);

    fwrite(&header, sizeof(struct bmp_header), 1, out);

    const size_t padding = 0;

    for (uint32_t line = 0; line < image->height; ++line) {
        fwrite(&(image->data[line * image->width]), sizeof(struct pixel), image->width, out);
        fwrite(&padding, 1, get_padding(image), out);
    }

    return WRITE_OK;
}
